package com.erminesoft.parser;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.Iterator;

/**
 * Created by dev on 1/31/17.
 */
public class SimpleParser {

    private static String stanza = "<message type=\"chat\" to=\"19@campfiire\" from=\"17@campfiire/2xai0u5z4d\"><body/><attachment>http://static3.businessinsider.com/image/5537f6f56bb3f740728fddb1-1190-625/26-beautiful-places-you-should-visit-before-they-disappear.jpg</attachment><timestamp>1485789379674</timestamp></message>";

    public static void main(String[] args) {
        addParametersToMessageFromStanza(stanza);
//        System.out.println(addParametersToMessageFromStanza(stanza));
    }

    private static String addParametersToMessageFromStanza(String stanza){
        Document document = null;
        try {
            document = DocumentHelper.parseText(stanza);
            Element root = document.getRootElement();
            for(Iterator i = root.elementIterator(); i.hasNext();){
                Element element = (Element) i.next();
                System.out.println("ELement`s name = " + element.getName() + ", value = " + element.getText());
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        return document.asXML();
    }
}
