package com.erminesoft.customiq;


import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class IQMucChat extends IQ{

    private String id;

    private  String owner;

    private String room_name;

    private String description;

    public IQMucChat(){
        super("query", "campfiire:iq:chat");
    }

    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {
        xmlBuilder.rightAngleBracket();
        xmlBuilder.openElement("owner").append(this.getOwner()).closeElement("owner");
        xmlBuilder.openElement("room_name").append(this.getRoom_name()).closeElement("room_name");
        xmlBuilder.openElement("description").append(this.getDescription()).closeElement("description");
        return xmlBuilder;
    }

    @Override
    protected void addCommonAttributes(XmlStringBuilder xml) {
        xml.optAttribute("id", this.getId());
        xml.optAttribute("action", "create");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
