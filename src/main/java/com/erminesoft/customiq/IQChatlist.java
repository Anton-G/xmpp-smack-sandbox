package com.erminesoft.customiq;


import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class IQChatlist extends IQ{

    private String id;

    private  String offset;

    private String count;

    public IQChatlist(){
        super("query", "campfiire:iq:chatlist");
    }

    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {
        xmlBuilder.rightAngleBracket();
        xmlBuilder.openElement("offset").append(this.getOffset()).closeElement("offset");
        xmlBuilder.openElement("count").append(this.getCount()).closeElement("count");
        return xmlBuilder;
    }

    @Override
    protected void addCommonAttributes(XmlStringBuilder xml) {
        xml.optAttribute("id", this.getId());
        xml.optAttribute("action", "chatlist");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
