package com.erminesoft.customiq;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

import java.util.ArrayList;

/**
 * Created by dev on 2/6/17.
 */
public class IQMucRoom extends IQ {

    private String id;

    private String mucId;

    private String action;

    private String name;

    private String description;

    private String mucJid;

    private ArrayList<String> peerList = new ArrayList<String>();

    private ArrayList<String> removeList = new ArrayList<String>();


    public IQMucRoom(){
        super("query", "campfiire:iq:muc");
    }

    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {
        xmlBuilder.rightAngleBracket();

        if(mucId != null){
            xmlBuilder.openElement("room-id").append(this.getMucId()).closeElement("room-id");
        }
        xmlBuilder.openElement("name").append(this.getName()).closeElement("name");
        xmlBuilder.openElement("description").append(this.getDescription()).closeElement("description");
        xmlBuilder.openElement("muc-jid").append(this.getMucJid()).closeElement("muc-jid");

        if(peerList.size() > 0){
            xmlBuilder.openElement("peer-list");
            for (String s : peerList) {
                xmlBuilder.openElement("peer").append(s).closeElement("peer");
            }
            xmlBuilder.closeElement("peer-list");
        }
        if(removeList.size() > 0){
            xmlBuilder.openElement("remove-list");
            for (String s : removeList) {
                xmlBuilder.openElement("peer").append(s).closeElement("peer");
            }
            xmlBuilder.closeElement("remove-list");
        }
        return xmlBuilder;
    }

    @Override
    protected void addCommonAttributes(XmlStringBuilder xml) {
        xml.optAttribute("id", this.getId());
        xml.optAttribute("action", this.getAction());
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMucId() {
        return mucId;
    }

    public void setMucId(String mucId) {
        this.mucId = mucId;
    }

    public String getMucJid() {
        return mucJid;
    }

    public void setMucJid(String mucJid) {
        this.mucJid = mucJid;
    }

    public ArrayList<String> getPeerList() {
        return peerList;
    }

    public void setPeerList(ArrayList<String> peerList) {
        this.peerList = peerList;
    }

    public ArrayList<String> getRemoveList() {
        return removeList;
    }

    public void setRemoveList(ArrayList<String> removeList) {
        this.removeList = removeList;
    }

    @Override
    public String toString() {
        return "IQMucRoom{" +
                "id='" + id + '\'' +
                ", mucId='" + mucId + '\'' +
                ", action='" + action + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", mucJid='" + mucJid + '\'' +
                ", peerList=" + peerList +
                '}';
    }
}
