package com.erminesoft.customiq;


import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class IQPrivateHistory extends IQ{

    private String id;
    private String recipient;
    private String action = "private_history";
    private String count = "0";
    private String offset = "10";

    public IQPrivateHistory(){
        super("query", "campfiire:iq:history");
    }

    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {
        xmlBuilder.rightAngleBracket();
        xmlBuilder.openElement("count").append(this.getCount()).closeElement("count");
        xmlBuilder.openElement("offset").append(this.getOffset()).closeElement("offset");
        return xmlBuilder;
    }

    @Override
    protected void addCommonAttributes(XmlStringBuilder xml) {
        xml.optAttribute("id", this.getId());
        xml.optAttribute("action", this.getAction());
        xml.optAttribute("recipient", this.getRecipient());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }
}
