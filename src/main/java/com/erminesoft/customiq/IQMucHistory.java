package com.erminesoft.customiq;


import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class IQMucHistory extends IQ{

    private String id;
    private String recipient;

    public IQMucHistory(){
        super("query", "campfiire:iq:history");
    }

    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {
        xmlBuilder.rightAngleBracket();
        xmlBuilder.openElement("count").append("10").closeElement("count");
        xmlBuilder.openElement("offset").append("0").closeElement("offset");
        return xmlBuilder;
    }

    @Override
    protected void addCommonAttributes(XmlStringBuilder xml) {
        xml.optAttribute("id", this.getId());
        xml.optAttribute("action", "muc_history");
        xml.optAttribute("recipient", this.getRecipient());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }
}
