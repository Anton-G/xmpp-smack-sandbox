package com.erminesoft.customiq;


import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class IQPrivateChat extends IQ{

    private String id;

    private  long chatId;

    private String action;

    private String peer;

    public IQPrivateChat(){
        super("query", "campfiire:iq:private-chat");
    }

    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {
        xmlBuilder.rightAngleBracket();
        xmlBuilder.openElement("chat-id").append(String.valueOf(this.getChatId())).closeElement("chat-id");
        xmlBuilder.openElement("peer").append(this.getPeer()).closeElement("peer");
        return xmlBuilder;
    }

    @Override
    protected void addCommonAttributes(XmlStringBuilder xml) {
        xml.optAttribute("id", this.getId());
        xml.optAttribute("action", this.getAction());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPeer() {
        return peer;
    }

    public void setPeer(String peer) {
        this.peer = peer;
    }
}
