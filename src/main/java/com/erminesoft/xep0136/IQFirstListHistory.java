package com.erminesoft.xep0136;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class IQFirstListHistory extends IQ{

    private String with;

    private String id;


    public IQFirstListHistory(String with) {
//        super("list", "urn:xmpp:archive");
//        super("list", "urn:xmpp:campfiire");
        super("query", "custom:iq:test");
        this.with = with;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {
//        xmlBuilder.optAttribute("with", with);
//        xmlBuilder.rightAngleBracket();
//        xmlBuilder.openElement("set").attribute("xmlns", "http://jabber.org/protocol/rsm")
//                .element("max", String.valueOf(30))
//                .closeElement("set");
        xmlBuilder.optAttribute("with", with);
        xmlBuilder.rightAngleBracket();
        xmlBuilder.halfOpenElement("set").append("'xmlns=http://jabber.org/protocol/rms'").rightAngleBracket().element("max", String.valueOf(30)).closeElement("set");
        return xmlBuilder;
    }

    @Override
    protected void addCommonAttributes(XmlStringBuilder xml) {
//        if(this.getWith() != null)
            xml.optAttribute("id", "ololo");
//        xml.optAttribute("id", this.getId());
    }


    //    public IQ addAttributeWith(String value){
//        this.addCommonAttributes()  ;
//    }

    public String getWith() {
        return with;
    }

    public void setWith(String with) {
        this.with = with;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
