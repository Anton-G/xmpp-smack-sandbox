package com.erminesoft.xep0136;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class IQsimple extends IQ{

    public IQsimple() {
        super("query", "custom:iq:Search");
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {

//        xmlBuilder.optAttribute("with", with);
        xmlBuilder.rightAngleBracket();
        xmlBuilder.openElement("search").append("helloo!!!!!").closeElement("search");
        return xmlBuilder;
    }

    @Override
    protected void addCommonAttributes(XmlStringBuilder xml) {
        xml.optAttribute("id", "search");
        xml.optAttribute("from", "trololo@campfiire");
    }

}
