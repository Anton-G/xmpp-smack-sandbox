package com.erminesoft;


import com.erminesoft.customiq.*;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.ChatManager;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.privacy.PrivacyListManager;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AppIQmsg {

    public static void main(String[] args) {

        XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();
//        config.setUsernameAndPassword("localhost_user_1","password");
        config.setUsernameAndPassword("4","password");
//        config.setUsernameAndPassword("chat_slave","password");
        config.setHost("127.0.0.1");
        config.setPort(5222);
//        config.setResource("campfiire");
        config.setServiceName("campfiire");
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

        AbstractXMPPConnection connection = new XMPPTCPConnection(config.build());

        try {
            System.out.println("connect");
            connection.connect();

            System.out.println("login");
            connection.login();

            System.out.println("create chat manager");
            ChatManager chatManager = ChatManager.getInstanceFor(connection);

//            System.out.println("create message");

//            Message message = new Message();
//            message.setFrom(connection.getUser());
//            message.setTo("newuser_1@campfiire");
//            message.setType(Message.Type.chat);
//            message.setBody("HELLLLLOOOOOOO!!!!! It`s me");

//            connection.sendStanza(message);


//            System.out.println("before IQ");
//            IQ myIq = new CustomIQ(3.14f);
//            myIq.setType(IQ.Type.get);
//            myIq.setTo("newuser_1@campfiire");
//            myIq.setFrom(connection.getUser());
//            connection.sendStanza(myIq);
//            System.out.println("after IQ");


//            System.out.println("SEND CUSTOM IQFirstListHistory");
//            IQ customIQ = new IQFirstListHistory("newuser_1@campfiire");
//            customIQ.setType(IQ.Type.get);
//            customIQ.setFrom(connection.getUser());
//            connection.sendStanza(customIQ);
//            System.out.println("after IQFirstListHistory");

//            IT WORKS
//            System.out.println("SEND IQSimple");
//            IQ simpleIq = new IQsimple();
//            simpleIq.setType(IQ.Type.get);
//            simpleIq.setFrom(connection.getUser());
//            connection.sendStanza(simpleIq);
//            System.out.println("AFTER SEND IQSimple");


////            WORKS WELL
//            System.out.println("SEND IQChatlist");
//            IQChatlist chatIq = new IQChatlist();
//            chatIq.setId("chatlisdt-23-2d55df");
//            chatIq.setFrom(connection.getUser());
//            chatIq.setOffset("0");
//            chatIq.setCount("100");
//            connection.sendStanza(chatIq);
//            System.out.println("AFTER SEND IQChatlist");

//+++
////            System.out.println("SEND IQHistory");
//            IQPrivateHistory historyIq = new IQPrivateHistory();
//            historyIq.setId("private-history-dfdsfsd99");
//            historyIq.setRecipient("3@campfiire");
//            historyIq.setFrom(connection.getUser());
//            connection.sendStanza(historyIq);
//            System.out.println("AFTER SEND IQHistory");

//            +++
//            System.out.println("SEND IQHistory remove");
//            IQPrivateHistory historyIq = new IQPrivateHistory();
//            historyIq.setId("private-history-dfdsfsd99");
//            historyIq.setRecipient("3@campfiire");
//            historyIq.setAction("private_history_delete");
//            historyIq.setFrom(connection.getUser());
//            connection.sendStanza(historyIq);
//            System.out.println("AFTER SEND IQHistory remove");

////+++
//            System.out.println("SEND IQHistory");
//            IQMucHistory mucIq = new IQMucHistory();
//            mucIq.setId("muc-history-opfdfd");
//            mucIq.setRecipient("ufz24khwbgcb23rw@conference.campfiire");
//            mucIq.setFrom(connection.getUser());
//            connection.sendStanza(mucIq);
//            System.out.println("AFTER SEND IQHistory");

//            System.out.println("Send create chat IQ");
//            IQMucChat mucChat = new IQMucChat();
//            mucChat.setId("dsf-3243f-ff");
//            mucChat.setOwner("admin");
//            mucChat.setRoom_name("my_5_room");
//            mucChat.setDescription("my_5_roor_description");
//            connection.sendStanza(mucChat);
//            System.out.println("After send create chat IQ");

//            System.out.println("SEND Create private chat");
//            IQPrivateChat prIq = new IQPrivateChat();
//            prIq.setType(IQ.Type.set);
//            prIq.setId("create-prw34ivate-chat-opdddfdfd");
//            prIq.setAction("create");
//            prIq.setPeer("6@campfiire");
//            connection.sendStanza(prIq);
//            System.out.println("AFTER Create private chat");


//            System.out.println("SEND Delete private chat");
//            IQPrivateChat prIqDelete = new IQPrivateChat();
//            prIqDelete.setType(IQ.Type.set);
//            prIqDelete.setId("delete-psrwi44vat233");
//            prIqDelete.setAction("delete");
//            prIqDelete.setChatId(3);
//            prIqDelete.setPeer("3@campfiire");
//            connection.sendStanza(prIqDelete);
//            System.out.println("AFTER Delete private chat");


////////////////            create muc chat
//            System.out.println("SEND Create MUC room");
//            IQMucRoom mucRoom = new IQMucRoom();
//            mucRoom.setType(IQ.Type.set);
//            mucRoom.setId("mduc-roomd-hhhh-11ss2223222");
//            mucRoom.setAction("create");
//            mucRoom.setName("zxc");
//            mucRoom.setDescription("zxc ROOM description");
//            mucRoom.setMucJid("some url for muc-room avatar");
//
//            mucRoom.getPeerList().add("3@campfiire");
//            mucRoom.getPeerList().add("2@campfiire");
//            connection.sendStanza(mucRoom);
//            System.out.println("AFTER send Create MUC room");
//            System.out.println(mucRoom);


//            System.out.println("SEND Delete MUC room");
//            IQMucRoom mucRoom = new IQMucRoom();
//            mucRoom.setType(IQ.Type.set);
//            mucRoom.setId("muc-room-hdhh3222");
//            mucRoom.setAction("user-exit");
//            mucRoom.setName("TEST_MUCroom_32");
//            mucRoom.setDescription("TEST_MUCroom_32 description");
//            mucRoom.setMucJid("ufz24khwbgcb23rw@conference.campfiire");
////
//            mucRoom.getPeerList().add("3@campfiire");
//            mucRoom.getPeerList().add("4@campfiire");
//            connection.sendStanza(mucRoom);
//            System.out.println("AFTER send Delete MUC room");
//            System.out.println(mucRoom);

//            System.out.println("SEND Add members to MUC room");
//            IQMucRoom mucRoom = new IQMucRoom();
//            mucRoom.setType(IQ.Type.set);
//            mucRoom.setId("mduc-room-hh2dd");
//            mucRoom.setAction("add-members");
//            mucRoom.setName("ufz24khwbgcb23rw@conference.campfiire");
//            mucRoom.setDescription("some description");
//            mucRoom.setMucJid("ufz24khwbgcb23rw@conference.campfiire");
//
//            mucRoom.getPeerList().add("4@campfiire");
//            mucRoom.getPeerList().add("5@campfiire");
//            mucRoom.getPeerList().add("6@campfiire");
//
////            mucRoom.getRemoveList().add("2@campfiire");
////            mucRoom.getRemoveList().add("5@campfiire");
////            mucRoom.getRemoveList().add("6@campfiire");
//            connection.sendStanza(mucRoom);
//            System.out.println("AFTER send Add members to MUC room");
//            System.out.println(mucRoom);


//            System.out.println("SEND EXIT from MUC room");
//            IQMucRoom mucRoom = new IQMucRoom();
//            mucRoom.setType(IQ.Type.set);
//            mucRoom.setId("mduc-room-dhh2");
//            mucRoom.setAction("user-exit");
//            mucRoom.setName("kisbo4hsvvmhl4zj@conference.campfiire");
//            mucRoom.setDescription("some description");
//            mucRoom.setMucJid("kisbo4hsvvmhl4zj@conference.campfiire");
//            connection.sendStanza(mucRoom);
//            System.out.println("AFTER EXIT from MUC room");
//            System.out.println(mucRoom);


//            !!!!! DOESNT WORK
//            System.out.println("BEFORE send XEP-0191");
//            XEP0191 xep0191 = new XEP0191();
//            xep0191.setType(IQ.Type.get);
//            xep0191.setId("disco1asdss");
//            xep0191.setTo("campfiire");
//            connection.sendStanza(xep0191);
//            System.out.println("AFTER send XEP-0191");

            PrivacyListManager privacyListManager = PrivacyListManager.getInstanceFor(connection);
            List<String> privacyList = new ArrayList<String>();

            if(privacyListManager != null){
                System.out.println("privacy lists numbers = " + privacyListManager.getPrivacyLists().size());
            }



//            boolean tempResult = blockFriend("3", connection);
//            System.out.println("tempResult : " + tempResult);



//            System.out.println("Active list name  : " + privacyListManager.getActiveListName());
//            System.out.println("Default list name  : " + privacyListManager.getDefaultListName());


//            System.out.println("\n\nprivacyListManager = " + privacyListManager);
//            if(privacyListManager != null){
//                System.out.println("privacy lists numbers = " + privacyListManager.getPrivacyLists().size());
//                privacyListManager.deletePrivacyList("trololo_list");
//            }


        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.getMessage();
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
    }

    private static boolean blockFriend(String friendName, AbstractXMPPConnection connection){
//        PrivacyItem item = new PrivacyItem(PrivacyItem.Type.jid, friendName, false, 7);
        PrivacyItem item = new PrivacyItem(PrivacyItem.Type.jid, friendName, false, 100);
        item.setFilterMessage(false);
        item.setFilterIQ(false);
        item.setFilterPresenceIn(false);
        item.setFilterPresenceOut(false);
        PrivacyListManager privacyListManager = PrivacyListManager.getInstanceFor(connection);
        List<PrivacyItem> list = new ArrayList<PrivacyItem>();
        list.add(item);

        System.out.println("\n\nITEM : " + item);
        System.out.println("Is allow : " + item.isAllow() );
        System.out.println("Is isFilterIQ : " + item.isFilterIQ());
        System.out.println("Is filterMessage : " + item.isFilterMessage());
        System.out.println("Is pressenceIn : " + item.isFilterPresenceIn());
        System.out.println("Is pressenceOut : " + item.isFilterPresenceOut());
        System.out.println("Is everything blocked : " + item.isFilterEverything() + "\n\n");
        System.out.println("To XML : " + item.toXML() + "\n\n");

        try {
            privacyListManager.updatePrivacyList("trololo_list", list);
            privacyListManager.setActiveListName("trololo_list");
            System.out.println("\n\n00000   ACTIVE LIST is = " + privacyListManager.getActiveListName());
            System.out.println("ololo");
            return true;
        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
            e.printStackTrace();
            return false;
        }
    }
}
