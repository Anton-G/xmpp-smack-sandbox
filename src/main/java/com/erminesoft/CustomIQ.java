package com.erminesoft;


import org.jivesoftware.smack.packet.IQ;

public class CustomIQ extends IQ {

    private float myData;


    protected CustomIQ(float myData) {
        super("query","urn:xmpp:mam:0");
        this.myData = myData;
    }


    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xmlBuilder) {
        xmlBuilder.rightAngleBracket();
        xmlBuilder.element("myData", String.valueOf(myData));
        xmlBuilder.element("deviceType", "vedroid waider");
        return xmlBuilder;
    }

//    public String getChildElementXML(){
//        String request = "<query xmlns='myxmlns>" +
//                "<myData>" + myData + "</myData>"
//                +"</query>";
//        return request;
//    }
}
