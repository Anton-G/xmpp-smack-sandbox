package com.erminesoft;


import com.erminesoft.customiq.IQChatlist;
import com.erminesoft.customiq.IQMucRoom;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.io.IOException;

public class AppIQmsgPROD {

    public static void main(String[] args) {

        XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();
//        config.setUsernameAndPassword("7","password");
//        config.setHost("34.195.42.117");
//        config.setPort(5222);
//        config.setServiceName("campfiire.gotests.com");
//        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

        AbstractXMPPConnection connection = new XMPPTCPConnection(config.build());

        try {
            System.out.println("connect");
            connection.connect();

            System.out.println("login");
            connection.login();

////////            WORKS WELL
//            System.out.println("SEND IQChatlist");
//            IQChatlist chatIq = new IQChatlist();
//            chatIq.setId("chatlist-23-2df");
//            chatIq.setFrom(connection.getUser());
//            chatIq.setOffset("0");
//            chatIq.setCount("20");
//            connection.sendStanza(chatIq);
//            System.out.println("AFTER SEND IQChatlist");


//            System.out.println("SEND IQHistory");
//            IQPrivateHistory historyIq = new IQPrivateHistory();
//            historyIq.setId("dfjksfs-df34f");
//            historyIq.setRecipient("user_for_test@campiire");
//            historyIq.setFrom(connection.getUser());
//            connection.sendStanza(historyIq);
//            System.out.println("AFTER SEND IQHistory");

//            System.out.println("SEND IQHistory");
//            IQMucHistory mucIq = new IQMucHistory();
//            mucIq.setId("olod-sds");
//            mucIq.setRecipient("qwerty@conference.campfiire");
//            mucIq.setFrom(connection.getUser());
//            connection.sendStanza(mucIq);
//            System.out.println("AFTER SEND IQHistory");


//            System.out.println("Send create chat IQ");
//            IQMucChat mucChat = new IQMucChat();
//            mucChat.setId("dsf-3243f-ff");
//            mucChat.setOwner("admin");
//            mucChat.setRoom_name("my_5_room");
//            mucChat.setDescription("my_5_roor_description");
//            connection.sendStanza(mucChat);
//            System.out.println("After send create chat IQ");


////            //////            create muc chat
//            System.out.println("SEND Create MUC room");
//            IQMucRoom mucRoom = new IQMucRoom();
//            mucRoom.setType(IQ.Type.set);
//            mucRoom.setId("msdasssdasd-11s2223222");
//            mucRoom.setAction("create");
//            mucRoom.setName("second test chat");
//            mucRoom.setDescription("second test chat description");
//            mucRoom.setMucJid("some url for muc-room avatar");
//
//            mucRoom.getPeerList().add("11@campfiire.gotests.com");
//            mucRoom.getPeerList().add("15@campfiire.gotests.com");
//            connection.sendStanza(mucRoom);
//            System.out.println("AFTER send Create MUC room");
//            System.out.println(mucRoom);


//            System.out.println("SEND IQChatlist");
//            IQChatlist chatIq = new IQChatlist();
//            chatIq.setId("chastl2d55dfDSSSD");
//            chatIq.setFrom(connection.getUser());
//            chatIq.setOffset("0");
//            chatIq.setCount("50");
//            connection.sendStanza(chatIq);
//            System.out.println("AFTER SEND IQChatlist");

//                        System.out.println("SEND Add members to MUC room");
//            IQMucRoom mucRoom = new IQMucRoom();
//            mucRoom.setType(IQ.Type.set);
//            mucRoom.setId("mduc-room-hh2");
//            mucRoom.setAction("add-members");
//            mucRoom.setName("0ugajtqmfq3snasx@conference.campfiire");
//            mucRoom.setDescription("some description");
//            mucRoom.setMucJid("jyl53xkqjoruitn5@conference.campfiire");
//
////            mucRoom.getPeerList().add("5@campfiire");
//            mucRoom.getPeerList().add("16@campfiire");
//
////            mucRoom.getRemoveList().add("5@campfiire");
////            mucRoom.getRemoveList().add("6@campfiire");
//            connection.sendStanza(mucRoom);
//            System.out.println("AFTER send Add members to MUC room");
//            System.out.println(mucRoom);




        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        }
        finally {
//            connection.disconnect();
        }
    }
}
